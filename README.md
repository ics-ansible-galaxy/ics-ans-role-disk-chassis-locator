# ics-ans-role-disk-chassis-locator

Ansible role to install the udev rule and script to generate EXXDYY devices under dev.

## Role Variables

```yaml
disk_chassis_locator_udev_priority: 60
disk_chassis_locator_script_path: /usr/local/bin/by-target.sh
disk_chassis_locator_trigger_udev: true
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-disk-chassis-locator
```

## License

BSD 2-clause
