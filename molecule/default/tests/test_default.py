import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_udev_rule(host):
    udevfile = host.file('/etc/udev/rules.d/42-persistent-disk-ports.rules')
    assert udevfile.is_file
    assert oct(udevfile.mode) == '0o644'
    assert udevfile.uid == 0
    assert udevfile.gid == 0


def test_udev_script(host):
    udevscript = host.file('/usr/bin/by-target_strange_path.sh')
    assert udevscript.is_file
    assert oct(udevscript.mode) == '0o755'
    assert udevscript.uid == 0
    assert udevscript.gid == 0
